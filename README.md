# Quick overview

Futuree is a so-called web application for collaboratively producing electronic
music sketches, and possibly even full tracks or albums. It uses the Web Audio
API and [Tone.js](https://tonejs.org) to generate sounds and
[Meteor](https://meteor.com) as the grand reactive backbone, which allows us to
share ideas and jam with minimal delay.

# Goal

Inspired by the designs of Teenage Engineering's OP-1 and their Pocket
Operators, we believe that intelligent limitations combined with non-traditional
graphics for representing states is a great way to spark musical creativity