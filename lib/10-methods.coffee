Meteor.methods
    clearEverything: ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Rooms.remove {}
        Components.remove {}
        Patterns.remove {}
        Messages.remove {}

    ###########
    ## Rooms ##
    ###########

    "Room.create": (name) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Rooms.insert
            name: name
            createdAt: new Date()
            owner: Meteor.userId()
            username: Meteor.user().username

    "Room.destroy": (id) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Rooms.remove id

    ################
    ## Components ##
    ################

    "Component.create": (type, roomId, state) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Components.insert
            type: type
            roomId: roomId
            state: state

    "Component.destroy": (id) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Components.remove id

    "Component.update.state": (id, newState) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Components.update id,
            $set: _.object(["state.#{key}", value] for own key, value of newState)

    ##############
    ## Patterns ##
    ##############

    "Pattern.create": (type, tags, name, pattern) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Patterns.insert
            userId: Meteor.userId()
            type: type
            tags: tags
            name: name
            pattern: pattern

    "Pattern.destroy": (id) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Patterns.remove id

    "Pattern.update.pattern": (id, newPattern) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Patterns.update id, {$set: {pattern: newPattern}}

    "Pattern.update.tags": (id, newTags) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Patterns.update id, {$set: {tags: newTags}}

    ##############
    ## Messages ##
    ##############

    "Message.create": (type, componentId, contents) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Messages.insert
            userId: Meteor.userId()
            type: type
            componentId: componentId
            contents: contents

    "Message.update.contents": (id, newContents) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")
        Messages.update id,
            $set: _.object(["contents.#{key}", value] for own key, value of newContents)

    # ###### #
    # .Queue #
    # ###### #

    "Message.Queue.createBetween": (schedulerId, componentId, prevSiblingId, nextSiblingId, pattern, name) ->
        if (!Meteor.userId())
            throw new Meteor.Error("not-authorized")

        prevSibling = Messages.findOne prevSiblingId
        nextSibling = Messages.findOne nextSiblingId
        if (prevSibling and prevSibling.type isnt "queue-pattern") or (nextSibling and nextSibling.type isnt "queue-pattern")
            throw new Meteor.Error("wrong-message-type")

        if (prevSibling and prevSibling.componentId isnt schedulerId) or (nextSibling and nextSibling.componentId isnt schedulerId)
            throw new Meteor.Error("wrong-message-component")
        if (prevSibling and prevSibling.contents.componentId isnt componentId) or (nextSibling and nextSibling.contents.componentId isnt componentId)
            throw new Meteor.Error("wrong-message-content-component")

        if prevSibling and nextSibling and prevSibling.contents.siblingId isnt nextSibling._id
            throw new Meteor.Error("message-not-between")
        if not nextSibling and prevSibling and prevSibling.contents.siblingId
            throw new Meteor.Error("message-has-sibling")

        scheduler = Components.findOne schedulerId
        try
            queueHead = Messages.findOne scheduler.state.queueHeads[componentId]
        catch error
        
        if queueHead and not prevSibling and queueHead._id isnt nextSiblingId
            console.log(queueHead)
            throw new Meteor.Error("message-isnt-head")

        Meteor.call "Message.create", "queue-pattern", schedulerId,
            componentId: componentId, pattern: pattern, siblingId: nextSiblingId, name: name
            (error, messageId) =>
                unless prevSibling
                    Meteor.call "Component.update.state", scheduler._id,
                        "queueHeads.#{componentId}": messageId
                else
                    Meteor.call "Message.update.contents", prevSibling._id,
                        "siblingId": messageId


    "Message.Queue.destroy": (id) ->
        unless Meteor.userId()
            throw new Meteor.Error("not-authorized")

        message = Messages.findOne id
        unless message
            throw new Meteor.Error("invalid-id")
        unless message.type is "queue-pattern"
            throw new Meteor.Error("wrong-message-type")

        scheduler = Components.findOne message.componentId
        try
            queueHead = Messages.findOne scheduler.state.queueHeads[componentId]
        catch error
        
        if queueHead and queueHead._id is id
            throw new Meteor.Error("message-is-head")

        prevSibling = Messages.findOne
            type: "queue-pattern"
            componentId: message.componentId
            "contents.siblingId": id
            "contents.componentId": message.contents.componentId
        prevSiblingId = if prevSibling then prevSibling._id else null

        Messages.remove id

        unless prevSibling
            Meteor.call "Component.update.state", message.componentId,
                "queueHeads.#{message.contents.componentId}": message.contents.siblingId
        else
            Meteor.call "Message.update.contents", prevSiblingId,
                "siblingId": message.contents.siblingId
