class Component
    constructor: ->
        @state = {}

    set: (newState) ->
        for own key, value of newState
            @[key] = value

@Component = Component
