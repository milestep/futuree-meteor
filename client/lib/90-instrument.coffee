class Instrument extends Component
    constructor: ->
        @empty =
            set: ->
            trigger: ->
        @notes = []
        Tone.Transport.setOnLoop @_parsePattern, 20

        @setup()
    setup: ->

    registerTrigger: (component, state) ->
        (time) =>
            try
                @[component].set state
                @[component].trigger time
            catch error
                    
    _parsePattern: (t) =>
        @clear()
        for own time, triggers of @queuedPattern.pattern
            for own component, state of triggers
                @notes.push Tone.Transport.setTimeline(@registerTrigger(component, state), time)

    clear: ->
        Tone.Transport.clearTimeline(id) for id in @notes
        @notes = []

@Instrument = Instrument
