class TheEngineV1 extends Instrument
    setup: ->
        @kick = new Kick()
        @snare = new Snare()
        @hihat = new Hihat()

class Kick extends Component
    constructor: ->
        @osc = new Tone.DrumSynth().toMaster()
        @noise = new Tone.NoiseSynth().toMaster()

        @noise.set
            envelope:
                attack: 0.01
                decay: 0.04
                sustain: 0
            filter:
                frequency: 10
                Q: 2
                type: "lowpass"
                rolloff: -12

        @note = "C1"
        @duration = "8n"

    trigger: (time) ->
        @osc.triggerAttackRelease(@note, @duration, time)
        @noise.triggerAttackRelease(@duration, time)
        
class Snare extends Component
    constructor: ->
        @osc = new Tone.FMSynth()
        @noise = new Tone.NoiseSynth()
        @filter = new Tone.Filter(1800, "lowpass").toMaster()

        @osc.set
            carrier:
                oscillator:
                    type: "pulse"
                    width: 0.01
                envelope:
                    attack: 0.01
                    decay: 0.15
                    sustain: 0

        @noise.set
            envelope:
                attack: 0.01
                decay: 0.2
                sustain: 0
            filter:
                Q: 0
                type: "highpass"
                rolloff: -12

        @osc.connect(@filter)
        @noise.connect(@filter)

        @note = "C1"
        @duration = "8n"

    trigger: (time) ->
        @osc.triggerAttackRelease(@note, @duration, time)
        @noise.triggerAttackRelease(@duration, time)

class Hihat extends Component
    constructor: ->
        @noise = new Tone.NoiseSynth().toMaster()

        @noise.set
            envelope:
                attack: 0.01
                decay: 0.04
                sustain: 0.0
            filter:
                Q: 6
                type: "highpass"
                rolloff: -12

        @duration = "8n"

    trigger: (time) ->
        @noise.triggerAttackRelease(@duration, time)
        
Template.the_engine_v1.helpers
    patterns: ->
        Patterns.find({userId: Meteor.userId(), type: {$in: [@type, 'generic']}})

    queue: ->
        Template.currentData().state.queuedPattern.name

Template.the_engine_v1.onRendered ->
    @plugin = new TheEngineV1()

    @autorun ->
        Template.instance().plugin.set(Components.findOne(Template.currentData()._id).state)

Template.the_engine_v1.onDestroyed ->
    @plugin.clear()

Template.the_engine_v1.events
    "click button.render": (event, template) ->
        template.renderer.render(template.score, template.sampler, "1:0")

    "click button.load": (event, template) ->
