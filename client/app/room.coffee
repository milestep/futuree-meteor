Template.room.events
    "click button.add-engine-v1": ->
        Meteor.call "Component.create", "the_engine_v1", @_id,
            queuedPattern:
                name: "empty"
                pattern: {}
            has_queue: true

    "click button.add-thruster-v1": ->
        Meteor.call "Component.create", "thruster_v1", @_id,
            queuedPattern:
                name: "empty"
                pattern: {}
            has_queue: true

    "click button.add-master-transport": ->
        Meteor.call "Component.create", "master_transport", @_id,
            bpm: 120
            timeSignature: 4
            loopLength: "1:0"

    "click button.add-scheduler": ->
        Meteor.call "Component.create", "scheduler", @_id, {queueHeads: {}}

    "click button.add-drum-pattern": ->
        Meteor.call "Pattern.create", "generic", [], "empty", {}

        Meteor.call "Pattern.create", "the_engine_v1", [], "base",
                "0:0":
                    kick:
                        note: "G1"
                "0:1":
                    hihat: {}
                "0:2":
                    snare: {}             
                "0:3":
                    hihat: {}             

        Meteor.call "Pattern.create", "the_engine_v1", [], "variation",
                "0:0":
                    kick:
                        note: "G1"
                "0:1:2":
                    kick:
                        note: "G1"
                "0:3:1":
                    kick:
                        note: "G1"
                "0:3:3":
                    kick:
                        note: "G1"
                "0:1":
                    hihat: {}
                "0:2":
                    snare: {}             
                "0:3":
                    hihat: {}             

    "click button.add-bass-pattern": ->
        Meteor.call "Pattern.create", "thruster_v1", [], "base",
                "0:0":
                    bass:
                        note: "G1"
                "0:1":
                    bass:
                        note: "C2"
                "0:1:2":
                    bass:
                        note: "F1"
                "0:2:0":
                    bass:
                        note: "A#1"
                "0:3":
                    bass:
                        note: "G1"
                "0:3:1":
                    bass:
                        note: "C1"
                "0:3:3":
                    bass:
                        note: "D1"

        Meteor.call "Pattern.create", "thruster_v1", [], "simple",
                "0:1":
                    bass:
                        note: "G1"
                "0:2":
                    bass:
                        note: "G1"
                "0:3:2":
                    bass:
                        note: "G1"

Template.room.helpers
    components: ->
        Components.find {roomId: @_id}, {sort: {type: -1}}
