class MasterTransport extends Component
    constructor: ->
        @context = Tone.context

        @bpm = 120
        @timeSignature = 4
        @loopLength = "1:0"

        Tone.Transport.loop = true
        Tone.Transport.setOnLoop @next, 40

    next: =>
        Meteor.call "Component.update.state", @_id,
            tick: not @tick

    update: ->
        Tone.Transport.bpm.value = @bpm
        Tone.Transport.timeSignature.value = @timeSignature
        Tone.Transport.setLoopPoints(0, @loopLength)

        if Tone.Transport.state isnt "started"
            if @lastTick and @lastTick is not @tick
                @start()
            @lastTick = @tick

    start: ->
        Tone.Transport.start()
        Session.set "transportState", "started"

    toggleStart: ->
        if Tone.Transport.state is "started"
            @stop()
        else
            @start()

    stop: ->
        Tone.Transport.stop()
        Session.set "transportState", "stopped"

    muteToggle: ->
        Tone.Master.mute = not Tone.Master.mute
        Session.set 'transportMuted', Tone.Master.mute

Session.set "transportState", Tone.Transport.state

Template.master_transport.onRendered ->
    @plugin = new MasterTransport()
    @plugin._id = @data._id

    @autorun ->
        Template.instance().plugin.set(Template.currentData().state)
        Template.instance().plugin.update()

Template.master_transport.onDestroyed ->
    @plugin.stop()

Template.master_transport.events
    'click button.play': (event, template) ->
        template.plugin.toggleStart()

    'click button.mute': (event, template) ->
        template.plugin.muteToggle()

    'click button.render':  (event, template) ->

    'submit .set-bpm': (event, template) ->
        event.preventDefault()
        Meteor.call "Component.update.state", template.data._id,
            bpm: event.target.bpm.value

Template.master_transport.helpers
    muted: ->
        if Session.get "transportMuted" then "unmute" else "mute"

    transportState: ->
        Session.get "transportState"

    bpm: ->
        Template.currentData().state.bpm
