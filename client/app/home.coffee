Template.home.events
    "submit .create-room": (event) ->
        event.preventDefault()
        name = event.target.text.value
        Meteor.call("Room.create", name)
        event.target.text.value = ""
    "click .delete": (event) ->
        Meteor.call("Room.destroy", @_id)

    "click .clear-everything": (event) ->
        Meteor.call("clearEverything")

Template.home.helpers
    rooms: ->
        Rooms.find {}, {sort: {createdAt: -1}}
