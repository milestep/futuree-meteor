Template.pattern_message.helpers
    get_name: ->
        console.log(Template.currentData().contents.patternId)
        Patterns.findOne(Template.currentData().contents.patternId).name

    notQueueHead: ->
        not (Components.findOne(Template.currentData().componentId).state.queueHeads[Template.currentData().contents.componentId] is Template.currentData()._id)

Template.pattern_message.events
    'click button.destroy': ->
        Meteor.call "Message.Queue.destroy", @_id

    'click button.duplicate': ->
        Meteor.call "Message.Queue.createBetween", @componentId, @contents.componentId, @_id, @contents.siblingId, @contents.pattern, @contents.name

Template.pattern_message.onRendered ->
    #console.log(@data.contents)
