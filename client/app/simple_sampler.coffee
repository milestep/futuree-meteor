class SimpleSampler
    constructor: ->
        @sample = new Tone.Player(nullBuffer).toMaster()
        @sample.buffer = nullBuffer
        @sample.retrigger = true

    setTimeline: ->
        @timeline_id = Tone.Transport.setTimeline(
            (time) =>
                @sample.start()
            "0:0")

    clearTimeline: ->
        Tone.Transport.clearTimeline(@timeline_id)
        
    setBuffer: (buffer) ->
        @sample.buffer = buffer

    clear: ->
        @sample.buffer = nullBuffer

    trimBuffer: ->
        channelData = @sample.buffer.getChannelData(channel)
        trim_start = @sample.buffer.getChannelData(channel).findIndex((val) -> val != 0) for channel in [0, 1]

@SimpleSampler = SimpleSampler
