class ThrusterV1 extends Instrument
    setup: ->
        @bass = new Bass()

class Bass extends Component
    constructor: ->
        @osc = new Tone.MonoSynth().toMaster()

        @osc.set
            filter:
                Q: -4
                type: "lowpass"
                rolloff: -24
            filterEnvelope:
                min: 40
                max: 200

        @note = "C2"
        @duration = "8n"

    trigger: (time) ->
        @osc.triggerAttackRelease(@note, @duration, time)

Template.thruster_v1.helpers
    patterns: ->
        Patterns.find({userId: Meteor.userId(), type: {$in: [@type, 'generic']}})

    queue: ->
        Template.currentData().state.queuedPattern.name

Template.thruster_v1.onRendered ->
    @plugin = new ThrusterV1()

    @autorun ->
        Template.instance().plugin.set(Components.findOne(Template.currentData()._id).state)

Template.thruster_v1.onDestroyed ->
    @plugin.clear()

Template.thruster_v1.events
    "click button.render": (event, template) ->
        template.renderer.render(template.score, template.sampler, "1:0")

    "click button.load": (event, template) ->
