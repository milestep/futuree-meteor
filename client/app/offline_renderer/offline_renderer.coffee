class OfflineRenderer
    constructor: (@plugin) ->

    render: (score, sampler, length) ->
        context = Tone.context
        offlineContext = new OfflineAudioContext(2,
            context.sampleRate * Tone.Transport.toSeconds(length),
            context.sampleRate)

        Tone.Transport.clearTimelines()
        Tone.setContext offlineContext

        temp_plugin = new @plugin()
        temp_plugin.parseScore(score)

        Tone.Transport.start()

        offlineContext.startRendering().then (renderedBuffer) ->
            Tone.Transport.stop()
            Tone.setContext context

            sampler.setBuffer(renderedBuffer)

            Tone.Transport.clearTimelines()
            sampler.setTimeline()

@OfflineRenderer = OfflineRenderer
