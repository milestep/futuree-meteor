class Scheduler extends Component
    constructor: ->
        @queueHeads = {}
        @consumedHeads = {}

        Tone.Transport.setOnLoop @next, 5

    next: (time) =>
        for own componentId, messageId of @queueHeads
            pattern = emptyPattern
            message = Messages.findOne(messageId)
            if @consumedHeads[componentId] is messageId
                if message
                    pattern = message.contents.pattern
                    siblingId = message.contents.siblingId

                    Meteor.call "Component.update.state", @_id,
                        "queueHeads.#{componentId}": siblingId
                    # Update the local queueHeads -- sync too slow
                    @queueHeads[componentId] = if siblingId then siblingId else null

                    if siblingId
                        pattern = Messages.findOne(siblingId).contents.pattern

                Meteor.call "Component.update.state", componentId, {queuedPattern: pattern}

            @consumedHeads[componentId] = @queueHeads[componentId]

Template.scheduler.onRendered ->
    @plugin = new Scheduler()
    @plugin._id = @data._id

    @autorun ->
        Template.instance().plugin.set(Template.currentData().state)

Template.scheduler.helpers
    template_id: ->
        Template.instance().data._id

    queues: ->
        Components.find({roomId: Template.currentData().roomId, "state.has_queue": true})

    queueFromComponent: (componentId) ->
        try
            messageId = Template.instance().data.state.queueHeads[componentId]
            while true
                unless messageId
                    break
                message = Messages.findOne(messageId)
                messageId = message.contents.siblingId
                message
        catch error
            return []
