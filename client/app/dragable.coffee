@drakes = {}

registerPatternDragables = (clss) ->
    for cls in clss
        do (cls) ->
            drakes[cls] = dragula
                isContainer: (el) ->
                    el.classList.contains("pattern-container") and el.dataset.type is cls
                direction: 'horizontal'
                copy: true#(el, source) ->
                    #source.classList.contains("pattern-source")
                accepts: (el, source) ->
                    not source.classList.contains("pattern-source")
                removeOnSpill: true

            drakes[cls].on "drop", (el, target, source, sibling) ->
                addPatternToContainer el, sibling, target

                # After getting all the data from the DOM element, we remove it
                # to let Blaze handle the structure.
                drakes[cls].remove()

            drakes[cls].on "remove", (el, container, source) ->
                if not source.classList.contains "pattern-source"
                    removePatternFromContainer el, source

addPatternToContainer = (el, sibling, container) ->
    scheduler = Components.findOne container.dataset.schedulerId
    componentId = container.dataset.componentId

    pattern = Patterns.findOne(el.dataset.id)
    if not pattern
        pattern = Messages.findOne(el.dataset.id).contents.pattern
        name = Messages.findOne(el.dataset.id).contents.name
    else
        name = pattern.name

    pos = _.indexOf(container.children, el)
    if el isnt container.lastChild
        nextSibling = container.children[pos + 1]
    if el isnt container.firstChild
        prevSibling = container.children[pos - 1]

    nextSiblingId = if nextSibling then nextSibling.dataset.id else null
    prevSiblingId = if prevSibling then prevSibling.dataset.id else null

    Meteor.call "Message.Queue.createBetween", scheduler._id, componentId, prevSiblingId, nextSiblingId, pattern, name

removePatternFromContainer = (el, container) ->
    Meteor.call "Message.Queue.destroy", el.dataset.id    

Template.body.onRendered ->
    registerPatternDragables ["the_engine_v1", "thruster_v1"]
