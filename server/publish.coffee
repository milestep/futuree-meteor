Meteor.publish "Rooms", ->
    Rooms.find {}

Meteor.publish "Components", ->
    Components.find {}

Meteor.publish "Patterns", ->
    Patterns.find {}

Meteor.publish "Messages", ->
    Messages.find {}
